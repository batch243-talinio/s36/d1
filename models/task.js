// create the Schema, model and exports the file
// imports the mongoose
	const mongoose = require("mongoose");

// Create the Schema using the mongoose.Schema() function
	const taskSchema = new mongoose.Schema({
		name: String,
		status:{
			type: String,
			default: "pending"
		}
	});

	module.exports = mongoose.model("Task", taskSchema);