const express = require("express");

// create a Router instance that functions as a middleware and routing sytem

const router = express.Router();

const taskController = require("../controllers/taskControllers")

// [Section] ROUTES
// Route to get all the tasks
// this route expects to recieve a GET request at the URL "/tasks"

	router.get("/", (req, res) => {
		// invokes the 'getAllTasks' function from the "taskController.js" file and send the result back to the client/Postman
			taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
	})


// Route to create a new task
// this route expects to reacieve a psot request at the URL "/tasks"
	
	router.post("/", (req, res) =>{
		// if information will be comin from the client side the data can be accessed from the request body
			taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
	})


// Route to Delete a task
	// this route expects to recieve a DELETE request at the url "/tasks/:id".
	// the colon (:) is an identifier from the URL, it helps create a dynamic route w/c allows us to supply information in the URL

		router.delete("/:id", (req, res) =>{

			taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController))
		})

// Route to Update a Task
	// this route expects to recieve a PUT request at the URL "/tasks/:id"

		router.put("/:id", (req, res) =>{

			taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
		})

// use "module.exports" to export the router object to use in the app.js
module.exports = router;